from socket import AI_NUMERICHOST
from flask import Flask, abort, request, jsonify, make_response, render_template
from flask_sqlalchemy import SQLAlchemy 
from flask_marshmallow import Marshmallow 
import os

from sqlalchemy import null

# Init app
app = Flask(__name__)
# Ukoliko je putanja u ovom root-u
# basedir = os.path.abspath(os.path.dirname(__file__))

# Database 
# app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql:///' + os.path.join(basedir, 'mysqlsampledatabase.sql')
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:Majlo123!@localhost/sample'
# gasimo pracenje izmjena zbog obavjestenja
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
# Init db
db = SQLAlchemy(app)
# Init ma 
ma = Marshmallow(app)

# Emplyees Class/Model 
class Employees(db.Model):
  employeeNumber = db.Column(db.Integer, primary_key=True)
  lastName = db.Column(db.String(100))
  firstName = db.Column(db.String(200))
  officeCode = db.Column(db.String(100))
  reportsTo = db.Column(db.Integer)
  jobTitle = db.Column(db.String(20))

  def __init__(self,employeeNumber, lastName, firstName, officeCode,reportsTo, jobTitle):
    self.employeeNumber = employeeNumber
    self.lastName = lastName
    self.firstName = firstName
    self.officeCode = officeCode
    self.reportsTo = reportsTo
    self.jobTitle = jobTitle



# Employees Schema
class EmployeesSchema(ma.Schema):
  class Meta:
    fields = ('employeeNumber', 'lastName', 'firstName', 'officeCode', 'reportsTo', 'jobTitle')

# Init schema
product_schema = EmployeesSchema()
products_schema = EmployeesSchema(many=True)

# # Error hadnler
# @app.errorhandler(500)
# def handle_500_error(e):
#       return make_response(jsonify({'error':'Not f'}),500)

# # Error hadnler
# @app.errorhandler(404)
# def handle_404_error(e):
#       return make_response(jsonify({'error':'Not f'}),404)  


@app.route('/')
def home():
      return render_template("index.html")
    

# Create Employee
@app.route('/employees', methods=['POST'])
def add_employee():
  try:    
    employeeNumber = request.json['employeeNumber']
    lastName = request.json['lastName']
    firstName = request.json['firstName']
    officeCode = request.json['officeCode']
    reportsTo = request.json['reportsTo']
    jobTitle = request.json['jobTitle']

    new_emp = Employees(employeeNumber,lastName, firstName, officeCode ,reportsTo, jobTitle)

    db.session.add(new_emp)
    db.session.commit()

    return product_schema.jsonify(new_emp)
  
  except Exception as e:
    return jsonify({"Error": "Invalid input"})
    
# Get All employees
@app.route('/employees', methods=['GET'])
def get_employee():
  all_emp = Employees.query.all()
  result = products_schema.dump(all_emp)
  return jsonify(result)

# Update employees
@app.route('/employees/<employeeNumber>', methods=['PUT'])
def update_employee(employeeNumber):
  emp = Employees.query.get(employeeNumber)

  lastName = request.json['lastName']
  firstName = request.json['firstName']
  officeCode = request.json['officeCode']
  reportsTo = request.json['reportsTo']
  jobTitle = request.json['jobTitle']

  emp.lastName = lastName
  emp.firstName = firstName
  emp.officeCode = officeCode
  emp.reportsTo = reportsTo
  emp.jobTitle = jobTitle

  db.session.commit()

  return product_schema.jsonify(emp)


# Delete 
@app.route('/employees/<employeeNumber>', methods=['DELETE'])
def delete_employee(employeeNumber):
  emp = Employees.query.get(employeeNumber)
  db.session.delete(emp)
  db.session.commit()

  return product_schema.jsonify(emp)


# Offices Class/Model
class Offices(db.Model):
  officeCode = db.Column(db.Integer, primary_key=True)
  city = db.Column(db.String(50))
  phone = db.Column(db.String(50))
  addressLine1 = db.Column(db.String(50))
  addressLine2 = db.Column(db.String(50))
  state = db.Column(db.String(50))
  country = db.Column(db.String(50))
  postalCode = db.Column(db.String(15))
  territory = db.Column(db.String(10))

  def __init__(self, city, phone, addressLine1,addressLine2, state,country, postalCode, territory):
    self.city = city
    self.phone = phone
    self.addressLine1 = addressLine1
    self.addressLine2 = addressLine2
    self.state = state
    self.country = country
    self.postalCode = postalCode
    self.territory = territory

# Product Schema
class OfficeSchema(ma.Schema):
  class Meta:
    fields = ('officeCode', 'city', 'phone', 'addressLine1', 'addressLine2', 'state','country','postalCode','territory')

# Init schema
office_schema = OfficeSchema()
offices_schema = OfficeSchema(many=True)

# Get All Offices
@app.route('/offices', methods=['GET'])
def get_offices():
  all_products2 = Offices.query.all()
  office = offices_schema.dump(all_products2)
  return jsonify(office)

# Get office by office code
@app.route('/offices/<officeCode>', methods=['GET'])
def get_code(officeCode):
  office = Offices.query.get(officeCode)
  return office_schema.jsonify(office)  

# db.create_all()

# Run Server
if __name__ == '__main__':
  app.run(debug=True)




