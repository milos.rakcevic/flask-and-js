

let myRequest = "http://127.0.0.1:5000/employees";
let officeRequest = "http://127.0.0.1:5000/offices";
let table = "";
const post2 = document.getElementById("rwd-table");   
const renderPost = (posts) => {
    table = `<tr>
    <th>Id</th>
    <th>First name</th>
    <th>Job title</th>
    <th>Last name</th>
    <th>Office code</th>
    <th>Reports to</th>
   
  </tr>`;
    
    posts.forEach((data) => {
        table += `
      <tr>
        <td>${data.employeeNumber}</td>
        <td>${data.firstName}</td>
        <td>${data.jobTitle}</td>
        <td>${data.lastName}</td>
        <td>${data.officeCode}</td>
        <td>${data.reportsTo}</td>
        <td> <a href="#" class="button" id="edit-post" data-id2=${data.employeeNumber} data-id=${data.employeeNumber+"_"+data.firstName+"_"+data.jobTitle+"_"+data.lastName+"_"+data.officeCode+"_"+data.reportsTo} >Edit</a></td>
        <td><a href="#" class="button" id="delete-post" data-id=${data.employeeNumber}>Delete</a></td>
        <td><a href="#" class="button" id="office-post" data-office=${data.officeCode}>Show office</a></td>
      </tr>`;
    })
    post2.innerHTML = table;
}

let en = document.getElementById("en");
let fname = document.getElementById("fname");
let jt = document.getElementById("jt")
let lname = document.getElementById("lname");
let oc = document.getElementById("oc");
let btnSubmit = document.querySelector(".sub");


// Get 
fetch(myRequest)
.then(res => res.json())
.then(data => renderPost(data))

// Kreiranje novih postova
post2.addEventListener('click', (e) => {
    e.preventDefault
    let delButtonPressed = e.target.id == 'delete-post';
    let editButtonPressed = e.target.id == 'edit-post';
    let id = e.target.dataset.id;
    let id2 = e.target.id2 == 'edit-post';
    id2 = e.target.dataset.id2;
    let OfficeCodePressed = e.target.offce = 'office-post';
    let officeCode = e.target.dataset.office;
    
    if(delButtonPressed) {
        fetch(`${myRequest}/${id}`,{
            method: 'DELETE',
        })
        .then(res => res.json())
        .then(() => location.reload())
    }
    // ukoliko se klikne dugme update uzimaju se podaci tog citavog reda i upisuju se u inpute
    // da bi se lakse izvrsio update radnika
    if(editButtonPressed) {
        let substring = id.split("_")
        
        var enumb = substring[0]
        var fn = substring[1];
        var jtitle = substring[2];
        var ln = substring[3];
        var ocode = substring[4];
        var rto = substring[5];
        
        en.value = enumb;
        fname.value = fn;
        jt.value = jtitle;
        lname.value = ln;
        oc.value = ocode;
        rt.value = rto;

    }
    // na dugme submit se izvrsava update zaposlenog
    btnSubmit.addEventListener('click', () => {
        fetch(`${myRequest}/${id2}`, {
         method: 'PUT',
         headers: {
             'Content-type': 'application/json'
         },
         body: JSON.stringify({
             employeeNumber: en.value,
             firstName: fname.value,
             jobTitle: jt.value,
             lastName: lname.value,
             officeCode: oc.value,
             reportsTo: rt.value
         })   
        })
        .then(res => res.json())
        .then(() => location.reload())
    })

    
    const post = document.getElementById("rwd-table2");

  
    if (OfficeCodePressed) {
        const renderPost = (posts) => {
            table = `<tr><th>City</th><th>Phone number</th><th>State</th><th>Country</th><th>Territory</th><th>Postal code</th><th>Address 1</th><th>Address 2</th></tr>`;
            posts.forEach((data) => {
                if(officeCode == data.officeCode)
                table += ` <tr><td class="id">  ${data.city} </td><td class="lastName" id="lastName"> ${data.phone}  </td><td class="firstName">  ${data.state} </td><td class="officeCode"> ${data.country} </td><td class="reportsTo">  ${data.territory} </td><td class="jobTitle"> ${data.postalCode} </td><td> ${data.addressLine1} </td><td> ${data.addressLine2} </td></tr> `;
            })
            post.innerHTML = table;
        }
        fetch(officeRequest)
        .then(res => res.json())
        .then(data => renderPost(data))
         }


})



